package com.note.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.note.master.entity.Notes;

public interface NotesRepository extends JpaRepository<Notes, Long>, JpaSpecificationExecutor<Notes>{

}
