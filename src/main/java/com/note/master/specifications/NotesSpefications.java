package com.note.master.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.note.master.entity.Notes;
import com.note.master.entity.Users;

@Component
public class NotesSpefications {
	
	
	public static Specification<Notes> byUser(Users user) {
        return (root, query, builder) -> builder.equal(root.get("users"), user);
    }

    public static Specification<Notes> byId(Long id) {
        return (root, query, builder) -> builder.equal(root.get("id"), id);
    }

}
