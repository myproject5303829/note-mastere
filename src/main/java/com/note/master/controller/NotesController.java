package com.note.master.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.note.master.entity.Notes;
import com.note.master.entity.dto.NotesDTO;
import com.note.master.entity.dto.PagerDto;
import com.note.master.service.NotesService;

@RestController
public class NotesController {

	@Autowired
	private NotesService notesService;

	@PostMapping
	public ResponseEntity<NotesDTO> saveNotes(@RequestBody NotesDTO notesDTO) {
		NotesDTO dto = this.notesService.saveNote(notesDTO);

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<NotesDTO> updateNotes(@RequestBody NotesDTO notesDTO) {
		NotesDTO dto = this.notesService.updateNote(notesDTO);

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@DeleteMapping("/id")
	public ResponseEntity<String> deleteNotes(Long id) {
		this.notesService.deleteNotes(id);

		return new ResponseEntity<>("notes Deleted sucessfully", HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<Object> getAllNotes(@RequestParam(required = false) Long id,Long userId,
			@RequestParam(defaultValue = "0") int pageNumber, @RequestParam(defaultValue = "10") int pageSize) {

		Map<String, Object> response = new HashMap<>();

		Page<Notes> notePage = this.notesService.getAllNotes(id, pageNumber, pageSize,userId);

		PagerDto pagerDto = new PagerDto();
		pagerDto.setTotalElements(notePage.getTotalElements());
		pagerDto.setTotalPages(notePage.getTotalPages());

		response.put("items", notePage);
		response.put("pager", pagerDto);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
