package com.note.master.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.note.master.entity.dto.UsersDto;
import com.note.master.service.UserService;

@RestController
public class UsersController {

	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<UsersDto> saveUsers(@RequestBody UsersDto usersDto) {
		UsersDto dto = this.userService.saveUsers(usersDto);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<UsersDto> getUsers() {
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
