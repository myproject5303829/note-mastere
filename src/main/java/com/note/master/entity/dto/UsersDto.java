package com.note.master.entity.dto;

import jakarta.validation.constraints.Email;

public class UsersDto {
	private Long id;
	
	private String firstName;
	
	private String lastName;
	
	@Email(message = "Email should be valid")
	private String email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
