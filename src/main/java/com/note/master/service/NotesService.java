package com.note.master.service;

import org.springframework.data.domain.Page;

import com.note.master.entity.Notes;
import com.note.master.entity.dto.NotesDTO;

public interface NotesService {

	public NotesDTO saveNote(NotesDTO notesDTO);

	public NotesDTO updateNote(NotesDTO notesDTO);

	public void deleteNotes(Long id);

	Page<Notes> getAllNotes(Long id, int pageNumber, int pageSize, Long userId);

}
