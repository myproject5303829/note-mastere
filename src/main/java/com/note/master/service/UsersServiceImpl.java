package com.note.master.service;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.note.master.entity.Users;
import com.note.master.entity.dto.UsersDto;
import com.note.master.exception.ResourceNotFoundException;
import com.note.master.repository.UserRepository;

public class UsersServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public UsersDto saveUsers(UsersDto usersDto) {

		Users users = this.modelMapper.map(usersDto, Users.class);

		Optional<Users> user = this.userRepository.findByEmail(usersDto.getEmail());

		if (user.isPresent()) {
			throw new ResourceNotFoundException("Users alredy exists by same id");

		}

		this.userRepository.save(users);

		UsersDto usersdto = this.modelMapper.map(this.userRepository.save(users), UsersDto.class);

		return usersdto;
	}

	public UsersDto getUsers(String email) {
		Optional<Users> user = this.userRepository.findByEmail(email);

		if (user.isEmpty()) {
			throw new ResourceNotFoundException("Users Not exists");
		}

		UsersDto usersdto = this.modelMapper.map(user.get(), UsersDto.class);
		return usersdto;

	}

}
