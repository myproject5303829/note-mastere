package com.note.master.service;

import java.time.LocalDateTime;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.note.master.entity.Notes;
import com.note.master.entity.Users;
import com.note.master.entity.dto.NotesDTO;
import com.note.master.exception.ResourceNotFoundException;
import com.note.master.repository.NotesRepository;
import com.note.master.repository.UserRepository;
import com.note.master.specifications.NotesSpefications;

public class NotesServiceImpl implements NotesService {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private NotesRepository notesRepository;
	
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public NotesDTO saveNote(NotesDTO notesDTO) {

		Notes notes = this.modelMapper.map(notesDTO, Notes.class);

		notes.setLastUpdated(LocalDateTime.now());

		NotesDTO dto = this.modelMapper.map(this.notesRepository.save(notes), NotesDTO.class);

		return dto;
	}

	@Override
	public NotesDTO updateNote(NotesDTO notesDTO) {

		Optional<Notes> notes = this.notesRepository.findById(notesDTO.getId());

		if (notes.isEmpty()) {
			throw new ResourceNotFoundException("Notes is not found");
		}
		Notes note = this.modelMapper.map(notesDTO, Notes.class);

		note.setLastUpdated(LocalDateTime.now());

		NotesDTO noteDTO = this.modelMapper.map(this.notesRepository.save(note), NotesDTO.class);

		return noteDTO;
	}

	@Override
	public void deleteNotes(Long id) {
		Optional<Notes> notes = this.notesRepository.findById(id);

		if (notes.isEmpty()) {
			throw new ResourceNotFoundException("Notes is not found");
		}

		this.notesRepository.delete(notes.get());

	}

	@Override
	public Page<Notes> getAllNotes(Long id, int pageNumber, int pageSize, Long userId) {

		Optional<Users> optionaluser = this.userRepository.findById(userId);
		if (optionaluser.isEmpty()) {
			throw new ResourceNotFoundException("User not found");
		}
		
		Specification<Notes> specification = Specification.where(null);
		
		specification = specification.and(NotesSpefications.byUser(optionaluser.get()));

		if (id != null) {
			specification = specification.and(NotesSpefications.byId(id));
		}

		PageRequest pageable = PageRequest.of(pageNumber, pageSize, Sort.by("id").ascending());
		Page<Notes> courseOutcomesPage = notesRepository.findAll(specification, pageable);
		return courseOutcomesPage;
	}

}
